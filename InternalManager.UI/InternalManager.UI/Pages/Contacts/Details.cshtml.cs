﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;

namespace InternalManager.UI.Pages.Contacts
{
    public class DetailsModel : PageModel
    {
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public DetailsModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        public Contact Contact { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Contact = await _context.Contacts
                .Include(c => c.Platform).FirstOrDefaultAsync(m => m.ContactId == id);

            if (Contact == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
