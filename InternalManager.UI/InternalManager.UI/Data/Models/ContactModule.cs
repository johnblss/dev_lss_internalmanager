﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class ContactModule
    {
        public byte[] Timestamp { get; set; }
        public long ContactModuleId { get; set; }
        public short ModuleId { get; set; }
        public long ContactId { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Module Module { get; set; }
    }
}
