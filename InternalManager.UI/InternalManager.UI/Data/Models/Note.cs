﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Note
    {
        public byte[] Timestamp { get; set; }
        public long NoteId { get; set; }
        public long? TicketId { get; set; }
        public long? ContactId { get; set; }
        public int? NoteType { get; set; }
        public int? MemberId { get; set; }
        public bool PublicNote { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
        public string TicketSubject { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Ticket Ticket { get; set; }
    }
}
