﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Template
    {
        public long TemplateId { get; set; }
        public long? EntityId { get; set; }
        public short? EntityTypeId { get; set; }
        public int TemplateTypeId { get; set; }
        public string TemplateContents { get; set; }
        public string Description { get; set; }
    }
}
