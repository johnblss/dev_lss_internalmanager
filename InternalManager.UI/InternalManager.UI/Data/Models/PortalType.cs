﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class PortalType
    {
        public PortalType()
        {
            Portals = new HashSet<Portal>();
        }

        public byte[] Timestamp { get; set; }
        public short PortalTypeId { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Portal> Portals { get; set; }
    }
}
