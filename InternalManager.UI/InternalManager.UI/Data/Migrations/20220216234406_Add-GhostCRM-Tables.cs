﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace InternalManager.UI.Migrations
{
    public partial class AddGhostCRMTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Group",
                columns: table => new
                {
                    GroupID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    GroupName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SecurityLevel = table.Column<short>(type: "smallint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Group", x => x.GroupID);
                });

            migrationBuilder.CreateTable(
                name: "Member",
                columns: table => new
                {
                    MemberID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Username = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Member", x => x.MemberID);
                });

            migrationBuilder.CreateTable(
                name: "Module",
                columns: table => new
                {
                    ModuleID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    ShortName = table.Column<string>(type: "varchar(5)", unicode: false, maxLength: 5, nullable: false),
                    Description = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true),
                    PlatformID = table.Column<short>(type: "smallint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Module", x => x.ModuleID);
                });

            migrationBuilder.CreateTable(
                name: "Platform",
                columns: table => new
                {
                    PlatformID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Name = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Platform", x => x.PlatformID);
                });

            migrationBuilder.CreateTable(
                name: "PortalType",
                columns: table => new
                {
                    PortalTypeID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PortalType", x => x.PortalTypeID);
                });

            migrationBuilder.CreateTable(
                name: "ProjectVersion",
                columns: table => new
                {
                    ProjectVersionId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    PlatformId = table.Column<byte>(type: "tinyint", nullable: false),
                    ProductVersion = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsReleased = table.Column<bool>(type: "bit", nullable: false),
                    DateReleased = table.Column<DateTime>(type: "datetime", nullable: false),
                    DatabaseVersion = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    ReleaseNotes = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectVersion", x => x.ProjectVersionId);
                });

            migrationBuilder.CreateTable(
                name: "Templates",
                columns: table => new
                {
                    TemplateID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EntityID = table.Column<long>(type: "bigint", nullable: true),
                    EntityTypeID = table.Column<short>(type: "smallint", nullable: true),
                    TemplateTypeID = table.Column<int>(type: "int", nullable: false),
                    TemplateContents = table.Column<string>(type: "xml", nullable: true),
                    Description = table.Column<string>(type: "varchar(50)", unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Templates", x => x.TemplateID);
                });

            migrationBuilder.CreateTable(
                name: "TicketCategory",
                columns: table => new
                {
                    CategoryID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Category = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketCategory", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "TicketPriority",
                columns: table => new
                {
                    PriorityID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Priority = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Priority", x => x.PriorityID);
                });

            migrationBuilder.CreateTable(
                name: "TicketStatus",
                columns: table => new
                {
                    StatusID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Status = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketStatus", x => x.StatusID);
                });

            migrationBuilder.CreateTable(
                name: "TicketType",
                columns: table => new
                {
                    TicketTypeID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketType", x => x.TicketTypeID);
                });

            migrationBuilder.CreateTable(
                name: "TicketVia",
                columns: table => new
                {
                    ViaID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Via = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketVia", x => x.ViaID);
                });

            migrationBuilder.CreateTable(
                name: "TutorialCategory",
                columns: table => new
                {
                    CategoryID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Category = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TutorialCategory", x => x.CategoryID);
                });

            migrationBuilder.CreateTable(
                name: "TutorialType",
                columns: table => new
                {
                    TutorialTypeID = table.Column<short>(type: "smallint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    Type = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TutorialType", x => x.TutorialTypeID);
                });

            migrationBuilder.CreateTable(
                name: "GroupMember",
                columns: table => new
                {
                    GroupMemberID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    GroupID = table.Column<short>(type: "smallint", nullable: true),
                    MemberID = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMember", x => x.GroupMemberID);
                    table.ForeignKey(
                        name: "FK_GroupMember_Group",
                        column: x => x.GroupID,
                        principalTable: "Group",
                        principalColumn: "GroupID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupMember_Member",
                        column: x => x.MemberID,
                        principalTable: "Member",
                        principalColumn: "MemberID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Contact",
                columns: table => new
                {
                    ContactID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    PlatformID = table.Column<short>(type: "smallint", nullable: true),
                    SalesRep = table.Column<int>(type: "int", nullable: true),
                    SupportRep = table.Column<int>(type: "int", nullable: true),
                    Company = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Address = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    City = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    State = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    Zip = table.Column<string>(type: "nvarchar(25)", maxLength: 25, nullable: true),
                    Country = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Website = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    QuickNotes = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CurrentSystem = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    CallBack = table.Column<DateTime>(type: "datetime", nullable: true),
                    SupportExp = table.Column<DateTime>(type: "datetime", nullable: true),
                    Sold = table.Column<bool>(type: "bit", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    GroupCodes = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Timezone = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SoldDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    InactiveDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Inactive = table.Column<bool>(type: "bit", nullable: false),
                    OtherDbas = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Seats = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contact", x => x.ContactID);
                    table.ForeignKey(
                        name: "FK_Contact_Platform",
                        column: x => x.PlatformID,
                        principalTable: "Platform",
                        principalColumn: "PlatformID",
                        onDelete: ReferentialAction.Restrict);

                    table.ForeignKey(
                        name: "FK_Contact_Platform",
                        column: x => x.PlatformID,
                        principalTable: "Platform",
                        principalColumn: "PlatformID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tutorial",
                columns: table => new
                {
                    TutorialID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    CategoryID = table.Column<short>(type: "smallint", nullable: true),
                    TutorialTypeID = table.Column<short>(type: "smallint", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: true),
                    URL = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    MemberID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tutorial", x => x.TutorialID);
                    table.ForeignKey(
                        name: "FK_Tutorial_Member",
                        column: x => x.MemberID,
                        principalTable: "Member",
                        principalColumn: "MemberID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tutorial_TutorialCategory",
                        column: x => x.CategoryID,
                        principalTable: "TutorialCategory",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tutorial_TutorialType",
                        column: x => x.TutorialTypeID,
                        principalTable: "TutorialType",
                        principalColumn: "TutorialTypeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CloudHosting",
                columns: table => new
                {
                    CHID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    ContactID = table.Column<long>(type: "bigint", nullable: true),
                    Server = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Username = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Password = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CloudHosting", x => x.CHID);
                    table.ForeignKey(
                        name: "FK_CloudHosting_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContactModule",
                columns: table => new
                {
                    ContactModuleID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    ModuleID = table.Column<short>(type: "smallint", nullable: false),
                    ContactID = table.Column<long>(type: "bigint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContactModule", x => x.ContactModuleID);
                    table.ForeignKey(
                        name: "FK_ContactModule_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ContactModule_Module",
                        column: x => x.ModuleID,
                        principalTable: "Module",
                        principalColumn: "ModuleID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "POC",
                columns: table => new
                {
                    POCID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    ContactID = table.Column<long>(type: "bigint", nullable: true),
                    Role = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    FirstName = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: true),
                    Email = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PhoneDescription = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Phone = table.Column<string>(type: "nvarchar(75)", maxLength: 75, nullable: true),
                    Ext = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    IsPrimary = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_POC", x => x.POCID);
                    table.ForeignKey(
                        name: "FK_POC_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Portal",
                columns: table => new
                {
                    PortalID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    ContactID = table.Column<long>(type: "bigint", nullable: false),
                    PortalTypeID = table.Column<short>(type: "smallint", nullable: true),
                    URL = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    StartDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    Active = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portal", x => x.PortalID);
                    table.ForeignKey(
                        name: "FK_Portal_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Portal_PortalType",
                        column: x => x.PortalTypeID,
                        principalTable: "PortalType",
                        principalColumn: "PortalTypeID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ticket",
                columns: table => new
                {
                    TicketID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    ContactID = table.Column<long>(type: "bigint", nullable: true),
                    LastDateModified = table.Column<DateTime>(type: "datetime", nullable: true),
                    ViaID = table.Column<short>(type: "smallint", nullable: true),
                    TicketTypeID = table.Column<short>(type: "smallint", nullable: true),
                    CategoryID = table.Column<short>(type: "smallint", nullable: true),
                    PriorityID = table.Column<short>(type: "smallint", nullable: true),
                    MemberIDFrom = table.Column<int>(type: "int", nullable: true),
                    MemberID = table.Column<int>(type: "int", nullable: true),
                    MonitorList = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StatusID = table.Column<short>(type: "smallint", nullable: true),
                    Subject = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Steps = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AdditionalInfo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateTicket = table.Column<DateTime>(type: "datetime", nullable: false),
                    BidAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    JobHours = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    ApprovedAmount = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    DateQuote = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateApproved = table.Column<DateTime>(type: "datetime", nullable: true),
                    DateSubmitted = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    Files = table.Column<string>(type: "xml", nullable: true),
                    PublicTicket = table.Column<bool>(type: "bit", nullable: false),
                    SentEmail = table.Column<bool>(type: "bit", nullable: false),
                    SentQuote = table.Column<bool>(type: "bit", nullable: false),
                    POCID = table.Column<long>(type: "bigint", nullable: false),
                    PrePaid = table.Column<bool>(type: "bit", nullable: true),
                    CurrentVersionId = table.Column<int>(type: "int", nullable: true),
                    FixedInVersionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ticket", x => x.TicketID);
                    table.ForeignKey(
                        name: "FK_Ticket_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_Member",
                        column: x => x.MemberID,
                        principalTable: "Member",
                        principalColumn: "MemberID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_POC",
                        column: x => x.POCID,
                        principalTable: "POC",
                        principalColumn: "POCID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_ProjectVersion",
                        column: x => x.CurrentVersionId,
                        principalTable: "ProjectVersion",
                        principalColumn: "ProjectVersionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_ProjectVersion1",
                        column: x => x.FixedInVersionId,
                        principalTable: "ProjectVersion",
                        principalColumn: "ProjectVersionId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_TicketCategory",
                        column: x => x.CategoryID,
                        principalTable: "TicketCategory",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_TicketPriority",
                        column: x => x.PriorityID,
                        principalTable: "TicketPriority",
                        principalColumn: "PriorityID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_TicketStatus",
                        column: x => x.StatusID,
                        principalTable: "TicketStatus",
                        principalColumn: "StatusID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_TicketType",
                        column: x => x.TicketTypeID,
                        principalTable: "TicketType",
                        principalColumn: "TicketTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ticket_TicketVia",
                        column: x => x.ViaID,
                        principalTable: "TicketVia",
                        principalColumn: "ViaID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Note",
                columns: table => new
                {
                    NoteID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    TicketID = table.Column<long>(type: "bigint", nullable: true),
                    ContactID = table.Column<long>(type: "bigint", nullable: true),
                    NoteType = table.Column<int>(type: "int", nullable: true),
                    MemberID = table.Column<int>(type: "int", nullable: true),
                    PublicNote = table.Column<bool>(type: "bit", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime", nullable: false),
                    TicketSubject = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Note", x => x.NoteID);
                    table.ForeignKey(
                        name: "FK_Note_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Note_Ticket",
                        column: x => x.TicketID,
                        principalTable: "Ticket",
                        principalColumn: "TicketID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TicketAudit",
                columns: table => new
                {
                    TicketAuditID = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    timestamp = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false),
                    TicketID = table.Column<long>(type: "bigint", nullable: false),
                    UserModifiedID = table.Column<int>(type: "int", nullable: false),
                    DateModified = table.Column<DateTime>(type: "datetime", nullable: true),
                    ContactID = table.Column<long>(type: "bigint", nullable: true),
                    IsContactID = table.Column<bool>(type: "bit", nullable: false),
                    ViaID = table.Column<short>(type: "smallint", nullable: true),
                    IsViaID = table.Column<bool>(type: "bit", nullable: false),
                    TicketTypeID = table.Column<short>(type: "smallint", nullable: true),
                    IsTicketTypeID = table.Column<bool>(type: "bit", nullable: false),
                    CategoryID = table.Column<short>(type: "smallint", nullable: true),
                    IsCategoryID = table.Column<bool>(type: "bit", nullable: false),
                    PriorityID = table.Column<short>(type: "smallint", nullable: true),
                    IsPriorityID = table.Column<bool>(type: "bit", nullable: false),
                    MemberIDFrom = table.Column<int>(type: "int", nullable: true),
                    IsMemberIDFrom = table.Column<bool>(type: "bit", nullable: false),
                    MemberID = table.Column<int>(type: "int", nullable: true),
                    IsMemberID = table.Column<bool>(type: "bit", nullable: false),
                    MonitorList = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsMonitorList = table.Column<bool>(type: "bit", nullable: false),
                    StatusID = table.Column<short>(type: "smallint", nullable: true),
                    IsStatusID = table.Column<bool>(type: "bit", nullable: false),
                    Subject = table.Column<string>(type: "nvarchar(255)", maxLength: 255, nullable: true),
                    IsSubject = table.Column<bool>(type: "bit", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDescription = table.Column<bool>(type: "bit", nullable: false),
                    Steps = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsSteps = table.Column<bool>(type: "bit", nullable: false),
                    AdditionalInfo = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsAdditionalInfo = table.Column<bool>(type: "bit", nullable: false),
                    DateTicket = table.Column<DateTime>(type: "datetime", nullable: false),
                    IsDateTicket = table.Column<bool>(type: "bit", nullable: false),
                    BidAmount = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    IsBidAmount = table.Column<bool>(type: "bit", nullable: false),
                    JobHours = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    IsJobHours = table.Column<bool>(type: "bit", nullable: false),
                    ApprovedAmount = table.Column<decimal>(type: "decimal(18,0)", nullable: true),
                    IsApprovedAmount = table.Column<bool>(type: "bit", nullable: false),
                    DateQuote = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDateQuote = table.Column<bool>(type: "bit", nullable: false),
                    DateApproved = table.Column<DateTime>(type: "datetime", nullable: true),
                    IsDateApproved = table.Column<bool>(type: "bit", nullable: false),
                    DateSubmitted = table.Column<DateTime>(type: "datetime", nullable: true, defaultValueSql: "(getdate())"),
                    IsDateSubmitted = table.Column<bool>(type: "bit", nullable: false),
                    Files = table.Column<string>(type: "xml", nullable: true),
                    IsFiles = table.Column<bool>(type: "bit", nullable: false),
                    PublicTicket = table.Column<bool>(type: "bit", nullable: false),
                    IsPublicTicket = table.Column<bool>(type: "bit", nullable: false),
                    SentEmail = table.Column<bool>(type: "bit", nullable: false),
                    IsSentEmail = table.Column<bool>(type: "bit", nullable: false),
                    SentQuote = table.Column<bool>(type: "bit", nullable: false),
                    IsSentQuote = table.Column<bool>(type: "bit", nullable: false),
                    POCID = table.Column<long>(type: "bigint", nullable: true),
                    IsPOCID = table.Column<bool>(type: "bit", nullable: false),
                    TicketNoteCount = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TicketAudit", x => x.TicketAuditID);
                    table.ForeignKey(
                        name: "FK_TicketAudit_Contact",
                        column: x => x.ContactID,
                        principalTable: "Contact",
                        principalColumn: "ContactID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_Member",
                        column: x => x.MemberID,
                        principalTable: "Member",
                        principalColumn: "MemberID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_Member1",
                        column: x => x.UserModifiedID,
                        principalTable: "Member",
                        principalColumn: "MemberID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_POC",
                        column: x => x.POCID,
                        principalTable: "POC",
                        principalColumn: "POCID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_Ticket",
                        column: x => x.TicketID,
                        principalTable: "Ticket",
                        principalColumn: "TicketID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_TicketCategory",
                        column: x => x.CategoryID,
                        principalTable: "TicketCategory",
                        principalColumn: "CategoryID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_TicketPriority",
                        column: x => x.PriorityID,
                        principalTable: "TicketPriority",
                        principalColumn: "PriorityID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_TicketStatus",
                        column: x => x.StatusID,
                        principalTable: "TicketStatus",
                        principalColumn: "StatusID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_TicketType",
                        column: x => x.TicketTypeID,
                        principalTable: "TicketType",
                        principalColumn: "TicketTypeID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TicketAudit_TicketVia",
                        column: x => x.ViaID,
                        principalTable: "TicketVia",
                        principalColumn: "ViaID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CloudHosting_ContactID",
                table: "CloudHosting",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_PlatformID",
                table: "Contact",
                column: "PlatformID");

            migrationBuilder.CreateIndex(
                name: "IX_ContactModule_ContactID",
                table: "ContactModule",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_ContactModule_ModuleID",
                table: "ContactModule",
                column: "ModuleID");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMember_GroupID",
                table: "GroupMember",
                column: "GroupID");

            migrationBuilder.CreateIndex(
                name: "IX_GroupMember_MemberID",
                table: "GroupMember",
                column: "MemberID");

            migrationBuilder.CreateIndex(
                name: "IX_Note_ContactID",
                table: "Note",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_Note_TicketID",
                table: "Note",
                column: "TicketID");

            migrationBuilder.CreateIndex(
                name: "IX_POC_ContactID",
                table: "POC",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_Portal_ContactID",
                table: "Portal",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_Portal_PortalTypeID",
                table: "Portal",
                column: "PortalTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_CategoryID",
                table: "Ticket",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_ContactID",
                table: "Ticket",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_CurrentVersionId",
                table: "Ticket",
                column: "CurrentVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_FixedInVersionId",
                table: "Ticket",
                column: "FixedInVersionId");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_MemberID",
                table: "Ticket",
                column: "MemberID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_POCID",
                table: "Ticket",
                column: "POCID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_PriorityID",
                table: "Ticket",
                column: "PriorityID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_StatusID",
                table: "Ticket",
                column: "StatusID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_TicketTypeID",
                table: "Ticket",
                column: "TicketTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_ViaID",
                table: "Ticket",
                column: "ViaID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_CategoryID",
                table: "TicketAudit",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_ContactID",
                table: "TicketAudit",
                column: "ContactID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_MemberID",
                table: "TicketAudit",
                column: "MemberID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_POCID",
                table: "TicketAudit",
                column: "POCID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_PriorityID",
                table: "TicketAudit",
                column: "PriorityID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_StatusID",
                table: "TicketAudit",
                column: "StatusID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_TicketID",
                table: "TicketAudit",
                column: "TicketID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_TicketTypeID",
                table: "TicketAudit",
                column: "TicketTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_UserModifiedID",
                table: "TicketAudit",
                column: "UserModifiedID");

            migrationBuilder.CreateIndex(
                name: "IX_TicketAudit_ViaID",
                table: "TicketAudit",
                column: "ViaID");

            migrationBuilder.CreateIndex(
                name: "IX_Tutorial_CategoryID",
                table: "Tutorial",
                column: "CategoryID");

            migrationBuilder.CreateIndex(
                name: "IX_Tutorial_MemberID",
                table: "Tutorial",
                column: "MemberID");

            migrationBuilder.CreateIndex(
                name: "IX_Tutorial_TutorialTypeID",
                table: "Tutorial",
                column: "TutorialTypeID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CloudHosting");

            migrationBuilder.DropTable(
                name: "ContactModule");

            migrationBuilder.DropTable(
                name: "GroupMember");

            migrationBuilder.DropTable(
                name: "Note");

            migrationBuilder.DropTable(
                name: "Portal");

            migrationBuilder.DropTable(
                name: "Templates");

            migrationBuilder.DropTable(
                name: "TicketAudit");

            migrationBuilder.DropTable(
                name: "Tutorial");

            migrationBuilder.DropTable(
                name: "Module");

            migrationBuilder.DropTable(
                name: "Group");

            migrationBuilder.DropTable(
                name: "PortalType");

            migrationBuilder.DropTable(
                name: "Ticket");

            migrationBuilder.DropTable(
                name: "TutorialCategory");

            migrationBuilder.DropTable(
                name: "TutorialType");

            migrationBuilder.DropTable(
                name: "Member");

            migrationBuilder.DropTable(
                name: "POC");

            migrationBuilder.DropTable(
                name: "ProjectVersion");

            migrationBuilder.DropTable(
                name: "TicketCategory");

            migrationBuilder.DropTable(
                name: "TicketPriority");

            migrationBuilder.DropTable(
                name: "TicketStatus");

            migrationBuilder.DropTable(
                name: "TicketType");

            migrationBuilder.DropTable(
                name: "TicketVia");

            migrationBuilder.DropTable(
                name: "Contact");

            migrationBuilder.DropTable(
                name: "Platform");
          
        }
    }
}
