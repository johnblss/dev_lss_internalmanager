﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TicketPriority
    {
        public TicketPriority()
        {
            TicketAudits = new HashSet<TicketAudit>();
            Tickets = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public short PriorityId { get; set; }
        public string Priority { get; set; }

        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
