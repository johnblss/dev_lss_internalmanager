﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TutorialType
    {
        public TutorialType()
        {
            Tutorials = new HashSet<Tutorial>();
        }

        public byte[] Timestamp { get; set; }
        public short TutorialTypeId { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Tutorial> Tutorials { get; set; }
    }
}
