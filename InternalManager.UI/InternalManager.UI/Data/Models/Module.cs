﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Module
    {
        public Module()
        {
            ContactModules = new HashSet<ContactModule>();
        }

        public byte[] Timestamp { get; set; }
        public short ModuleId { get; set; }
        public string ShortName { get; set; }
        public string Description { get; set; }
        public short? PlatformId { get; set; }

        public virtual ICollection<ContactModule> ContactModules { get; set; }
    }
}
