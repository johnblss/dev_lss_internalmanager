﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TicketStatus
    {
        public TicketStatus()
        {
            TicketAudits = new HashSet<TicketAudit>();
            Tickets = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public short StatusId { get; set; }
        public string Status { get; set; }

        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
