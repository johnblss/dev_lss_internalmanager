﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Contact
    {
        public Contact()
        {
            CloudHostings = new HashSet<CloudHosting>();
            ContactModules = new HashSet<ContactModule>();
            NotesNavigation = new HashSet<Note>();
            Pocs = new HashSet<Poc>();
            Portals = new HashSet<Portal>();
            TicketAudits = new HashSet<TicketAudit>();
            Tickets = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public long ContactId { get; set; }
        public short? PlatformId { get; set; }

        [Display(Name = "Sales Rep")]
        public int? SalesRep { get; set; }

        [Display(Name = "Support Rep")]
        public int? SupportRep { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Website { get; set; }

        [Display(Name = "Quick Notes")]
        public string QuickNotes { get; set; }

        [Display(Name = "Quick Notes")]
        public string QuickNotesLimited
        {
            get 
            {
                return (QuickNotes == null || QuickNotes.Length < 40) ? QuickNotes : QuickNotes.Substring(0, 40) + "...";

            }
        }

        public string Notes { get; set; }
        public string CurrentSystem { get; set; }
        public DateTime? CallBack { get; set; }

        [Display(Name = "Expired")]
        [DataType(DataType.Date)]
        public DateTime? SupportExp { get; set; }
        public bool Sold { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string GroupCodes { get; set; }
        public string Timezone { get; set; }

        [Display(Name = "Sold Date")]
        [DataType(DataType.Date)]
        public DateTime? SoldDate { get; set; }
        public DateTime? InactiveDate { get; set; }
        public bool Inactive { get; set; }
        public string OtherDbas { get; set; }
        public string Seats { get; set; }

        public Poc DefaultPOC
        {
            get 
            {
                return Pocs.Where(poc => poc.IsPrimary).FirstOrDefault();
            }
        }


        public virtual Platform Platform { get; set; }
        public virtual Member SalesRepresentative { get; set; }

        public virtual Member SupportRepresentative { get; set; }

        public virtual ICollection<CloudHosting> CloudHostings { get; set; }
        public virtual ICollection<ContactModule> ContactModules { get; set; }
        public virtual ICollection<Note> NotesNavigation { get; set; }
        public virtual ICollection<Poc> Pocs { get; set; }
        public virtual ICollection<Portal> Portals { get; set; }
        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
