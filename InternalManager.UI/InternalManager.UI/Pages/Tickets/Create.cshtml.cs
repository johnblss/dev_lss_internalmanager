﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;

namespace InternalManager.UI.Pages.Tickets
{
    public class CreateModel : PageModel
    {
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public CreateModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CategoryId"] = new SelectList(_context.TicketCategories, "CategoryId", "CategoryId");
        ViewData["ContactId"] = new SelectList(_context.Contacts, "ContactId", "ContactId");
        ViewData["CurrentVersionId"] = new SelectList(_context.ProjectVersions, "ProjectVersionId", "DatabaseVersion");
        ViewData["FixedInVersionId"] = new SelectList(_context.ProjectVersions, "ProjectVersionId", "DatabaseVersion");
        ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "MemberId");
        ViewData["Pocid"] = new SelectList(_context.Pocs, "Pocid", "Pocid");
        ViewData["PriorityId"] = new SelectList(_context.TicketPriorities, "PriorityId", "PriorityId");
        ViewData["StatusId"] = new SelectList(_context.TicketStatuses, "StatusId", "StatusId");
        ViewData["TicketTypeId"] = new SelectList(_context.TicketTypes, "TicketTypeId", "TicketTypeId");
        ViewData["ViaId"] = new SelectList(_context.TicketVia, "ViaId", "ViaId");
            return Page();
        }

        [BindProperty]
        public Ticket Ticket { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Tickets.Add(Ticket);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
