﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Poc
    {
        public Poc()
        {
            TicketAudits = new HashSet<TicketAudit>();
            Tickets = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public long Pocid { get; set; }
        public long? ContactId { get; set; }
        public string Role { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }

        [Display(Name = "E-Mail")]
        public string Email { get; set; }

       
        public string PhoneDescription { get; set; }

        [Display(Name = "Phone #")]
        public string Phone { get; set; }
        public string Ext { get; set; }
        public bool IsPrimary { get; set; }

        [Display(Name = "Name")]
        public string FullName 
        {
            get {
                return FirstName + " " + LastName;
            } 
        }

        public virtual Contact Contact { get; set; }
        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
