﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class ProjectVersion
    {
        public ProjectVersion()
        {
            TicketCurrentVersions = new HashSet<Ticket>();
            TicketFixedInVersions = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public int ProjectVersionId { get; set; }
        public byte PlatformId { get; set; }
        public string ProductVersion { get; set; }
        public bool IsReleased { get; set; }
        public DateTime DateReleased { get; set; }
        public string DatabaseVersion { get; set; }
        public string ReleaseNotes { get; set; }

        public virtual ICollection<Ticket> TicketCurrentVersions { get; set; }
        public virtual ICollection<Ticket> TicketFixedInVersions { get; set; }
    }
}
