﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Group
    {
        public Group()
        {
            GroupMembers = new HashSet<GroupMember>();
        }

        public byte[] Timestamp { get; set; }
        public short GroupId { get; set; }
        public string GroupName { get; set; }
        public short? SecurityLevel { get; set; }

        public virtual ICollection<GroupMember> GroupMembers { get; set; }
    }
}
