﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Platform
    {
        public Platform()
        {
            Contacts = new HashSet<Contact>();
        }

        public byte[] Timestamp { get; set; }
        public short PlatformId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
