﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TicketVium
    {
        public TicketVium()
        {
            TicketAudits = new HashSet<TicketAudit>();
            Tickets = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public short ViaId { get; set; }
        public string Via { get; set; }

        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
