﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using log4net;

namespace InternalManager.UI.Pages.Contacts
{
    public class IndexModel : PageModel
    {
        private readonly ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public IndexModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        public IList<Contact> Contact { get;set; }

        public async Task OnGetAsync()
        {
            try
            {
                logger.Debug("In OnGetAsync");

                // Use LINQ to get list of platforms.
                IQueryable<string> platformQuery = from pl in _context.Platforms select pl.Name;

                var tmpCustomers = from c in _context.Contacts select c;

                if (!string.IsNullOrEmpty(SearchString))
                {
                    tmpCustomers = tmpCustomers.Where(c => c.Company.Contains(SearchString));
                }

                if (!string.IsNullOrEmpty(SearchString))
                {
                    tmpCustomers = tmpCustomers.Where(c => c.Company.Contains(SearchString));
                }

                if (!string.IsNullOrEmpty(ContactPlatform))
                {
                    tmpCustomers = tmpCustomers.Where(x => x.Platform.Name == ContactPlatform);
                }

                Platforms = new SelectList(platformQuery.ToList());
                Contact = await tmpCustomers.Include(c => c.SalesRepresentative).Include(c => c.SupportRepresentative).Include(c => c.Pocs).Include(c => c.Platform).Take(100).ToListAsync();
            }
            catch (Exception exc)
            {
                logger.Error(exc);
            }
        }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        public SelectList Platforms { get; set; }
        [BindProperty(SupportsGet = true)]
        public string ContactPlatform { get; set; }
    }
}
