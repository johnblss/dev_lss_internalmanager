﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Member
    {
        public Member()
        {
            GroupMembers = new HashSet<GroupMember>();
            TicketAuditMembers = new HashSet<TicketAudit>();
            TicketAuditUserModifieds = new HashSet<TicketAudit>();            
            Tutorials = new HashSet<Tutorial>();
            SalesRepContacts = new HashSet<Contact>();
            SupportRepContacts = new HashSet<Contact>();
            ReceivedTickets = new HashSet<Ticket>();
            SentTickets = new HashSet<Ticket>();

        }

        public byte[] Timestamp { get; set; }
        public int MemberId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? StartDate { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        [Display(Name = "Name")]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public virtual ICollection<GroupMember> GroupMembers { get; set; }
        public virtual ICollection<TicketAudit> TicketAuditMembers { get; set; }
        public virtual ICollection<TicketAudit> TicketAuditUserModifieds { get; set; }
        /*public virtual ICollection<Ticket> Tickets { get; set; }*/
        public virtual ICollection<Tutorial> Tutorials { get; set; }
        public virtual ICollection<Contact> SalesRepContacts { get; set; }
        public virtual ICollection<Contact> SupportRepContacts { get; set; }
        public virtual ICollection<Ticket> ReceivedTickets { get; set; }
        public virtual ICollection<Ticket> SentTickets { get; set; }

    }
}
