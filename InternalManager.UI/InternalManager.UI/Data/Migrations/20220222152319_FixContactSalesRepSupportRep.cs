﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternalManager.UI.Migrations
{
    public partial class FixContactSalesRepSupportRep : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Contact_SalesRep",
                table: "Contact",
                column: "SalesRep");

            migrationBuilder.CreateIndex(
                name: "IX_Contact_SupportRep",
                table: "Contact",
                column: "SupportRep");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_SalesRepresentative",
                table: "Contact",
                column: "SalesRep",
                principalTable: "Member",
                principalColumn: "MemberID");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_SupportRepresentative",
                table: "Contact",
                column: "SupportRep",
                principalTable: "Member",
                principalColumn: "MemberID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact_SalesRepresentative",
                table: "Contact");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact_SupportRepresentative",
                table: "Contact");

            migrationBuilder.DropIndex(
                name: "IX_Contact_SalesRep",
                table: "Contact");

            migrationBuilder.DropIndex(
                name: "IX_Contact_SupportRep",
                table: "Contact");
        }
    }
}
