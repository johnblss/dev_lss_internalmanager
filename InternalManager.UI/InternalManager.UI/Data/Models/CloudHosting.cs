﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class CloudHosting
    {
        public byte[] Timestamp { get; set; }
        public long Chid { get; set; }
        public long? ContactId { get; set; }
        public string Server { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public DateTime? StartDate { get; set; }
        public bool Active { get; set; }

        public virtual Contact Contact { get; set; }
    }
}
