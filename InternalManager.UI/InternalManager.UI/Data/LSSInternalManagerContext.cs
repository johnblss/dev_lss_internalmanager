﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using InternalManager.UI.Data.Models;

#nullable disable

namespace InternalManager.UI.Data
{
    public partial class LSSInternalManagerContext : DbContext
    {
        public LSSInternalManagerContext()
        {
        }
             

        public LSSInternalManagerContext(DbContextOptions<LSSInternalManagerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CloudHosting> CloudHostings { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<ContactModule> ContactModules { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupMember> GroupMembers { get; set; }
        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Note> Notes { get; set; }
        public virtual DbSet<Platform> Platforms { get; set; }
        public virtual DbSet<Poc> Pocs { get; set; }
        public virtual DbSet<Portal> Portals { get; set; }
        public virtual DbSet<PortalType> PortalTypes { get; set; }
        public virtual DbSet<ProjectVersion> ProjectVersions { get; set; }
        public virtual DbSet<Template> Templates { get; set; }
        public virtual DbSet<Ticket> Tickets { get; set; }
        public virtual DbSet<TicketAudit> TicketAudits { get; set; }
        public virtual DbSet<TicketCategory> TicketCategories { get; set; }
        public virtual DbSet<TicketPriority> TicketPriorities { get; set; }
        public virtual DbSet<TicketStatus> TicketStatuses { get; set; }
        public virtual DbSet<TicketType> TicketTypes { get; set; }
        public virtual DbSet<TicketVium> TicketVia { get; set; }
        public virtual DbSet<Tutorial> Tutorials { get; set; }
        public virtual DbSet<TutorialCategory> TutorialCategories { get; set; }
        public virtual DbSet<TutorialType> TutorialTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server=(local);Database=LSSInternalManager;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<CloudHosting>(entity =>
            {
                entity.HasKey(e => e.Chid);

                entity.ToTable("CloudHosting");

                entity.Property(e => e.Chid).HasColumnName("CHID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.Server).HasMaxLength(100);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Username).HasMaxLength(50);

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.CloudHostings)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_CloudHosting_Contact");
            });

            modelBuilder.Entity<Contact>(entity =>
            {
                entity.ToTable("Contact");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Address).HasMaxLength(255);

                entity.Property(e => e.CallBack).HasColumnType("datetime");

                entity.Property(e => e.City).HasMaxLength(50);

                entity.Property(e => e.Company).HasMaxLength(255);

                entity.Property(e => e.Country).HasMaxLength(50);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CurrentSystem).HasMaxLength(255);

                entity.Property(e => e.GroupCodes).HasMaxLength(255);

                entity.Property(e => e.InactiveDate).HasColumnType("datetime");

                entity.Property(e => e.OtherDbas).HasMaxLength(255);

                entity.Property(e => e.PlatformId).HasColumnName("PlatformID");

                entity.Property(e => e.QuickNotes).HasMaxLength(255);

                entity.Property(e => e.Seats).HasMaxLength(10);

                entity.Property(e => e.SoldDate).HasColumnType("datetime");

                entity.Property(e => e.State).HasMaxLength(15);

                entity.Property(e => e.SupportExp).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Timezone).HasMaxLength(50);

                entity.Property(e => e.Website).HasMaxLength(255);

                entity.Property(e => e.Zip).HasMaxLength(25);

                entity.Property(e => e.SalesRep).HasColumnName("SalesRep");

                entity.Property(e => e.SupportRep).HasColumnName("SupportRep");

                entity.HasOne(d => d.Platform)
                    .WithMany(p => p.Contacts)
                    .HasForeignKey(d => d.PlatformId)
                    .HasConstraintName("FK_Contact_Platform");

                entity.HasOne(d => d.SalesRepresentative)
                    .WithMany(p => p.SalesRepContacts)
                    .HasForeignKey(d => d.SalesRep)
                    .HasConstraintName("FK_Contact_SalesRepresentative");

                entity.HasOne(d => d.SupportRepresentative)
                    .WithMany(p => p.SupportRepContacts)
                    .HasForeignKey(d => d.SupportRep)
                    .HasConstraintName("FK_Contact_SupportRepresentative");
            });

            modelBuilder.Entity<ContactModule>(entity =>
            {
                entity.ToTable("ContactModule");

                entity.Property(e => e.ContactModuleId).HasColumnName("ContactModuleID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.ContactModules)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContactModule_Contact");

                entity.HasOne(d => d.Module)
                    .WithMany(p => p.ContactModules)
                    .HasForeignKey(d => d.ModuleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ContactModule_Module");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("Group");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.GroupName).HasMaxLength(50);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<GroupMember>(entity =>
            {
                entity.ToTable("GroupMember");

                entity.Property(e => e.GroupMemberId).HasColumnName("GroupMemberID");

                entity.Property(e => e.GroupId).HasColumnName("GroupID");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupMembers)
                    .HasForeignKey(d => d.GroupId)
                    .HasConstraintName("FK_GroupMember_Group");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.GroupMembers)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_GroupMember_Member");
            });

            modelBuilder.Entity<Member>(entity =>
            {
                entity.ToTable("Member");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Username).HasMaxLength(50);
            });

            modelBuilder.Entity<Module>(entity =>
            {
                entity.ToTable("Module");

                entity.Property(e => e.ModuleId).HasColumnName("ModuleID");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PlatformId).HasColumnName("PlatformID");

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<Note>(entity =>
            {
                entity.ToTable("Note");

                entity.Property(e => e.NoteId).HasColumnName("NoteID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.TicketId).HasColumnName("TicketID");

                entity.Property(e => e.TicketSubject).HasMaxLength(255);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.NotesNavigation)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_Note_Contact");

                entity.HasOne(d => d.Ticket)
                    .WithMany(p => p.Notes)
                    .HasForeignKey(d => d.TicketId)
                    .HasConstraintName("FK_Note_Ticket");
            });

            modelBuilder.Entity<Platform>(entity =>
            {
                entity.ToTable("Platform");

                entity.Property(e => e.PlatformId).HasColumnName("PlatformID");

                entity.Property(e => e.Name).HasMaxLength(50);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<Poc>(entity =>
            {
                entity.ToTable("POC");

                entity.Property(e => e.Pocid).HasColumnName("POCID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Ext).HasMaxLength(20);

                entity.Property(e => e.FirstName).HasMaxLength(75);

                entity.Property(e => e.LastName).HasMaxLength(75);

                entity.Property(e => e.Phone).HasMaxLength(75);

                entity.Property(e => e.PhoneDescription).HasMaxLength(50);

                entity.Property(e => e.Role).HasMaxLength(255);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Pocs)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_POC_Contact");
            });

            modelBuilder.Entity<Portal>(entity =>
            {
                entity.ToTable("Portal");

                entity.Property(e => e.PortalId).HasColumnName("PortalID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.PortalTypeId).HasColumnName("PortalTypeID");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .HasColumnName("URL");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Portals)
                    .HasForeignKey(d => d.ContactId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Portal_Contact");

                entity.HasOne(d => d.PortalType)
                    .WithMany(p => p.Portals)
                    .HasForeignKey(d => d.PortalTypeId)
                    .HasConstraintName("FK_Portal_PortalType");
            });

            modelBuilder.Entity<PortalType>(entity =>
            {
                entity.ToTable("PortalType");

                entity.Property(e => e.PortalTypeId).HasColumnName("PortalTypeID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            modelBuilder.Entity<ProjectVersion>(entity =>
            {
                entity.ToTable("ProjectVersion");

                entity.Property(e => e.DatabaseVersion)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DateReleased).HasColumnType("datetime");

                entity.Property(e => e.ProductVersion).HasMaxLength(50);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<Template>(entity =>
            {
                entity.Property(e => e.TemplateId).HasColumnName("TemplateID");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.EntityTypeId).HasColumnName("EntityTypeID");

                entity.Property(e => e.TemplateContents).HasColumnType("xml");

                entity.Property(e => e.TemplateTypeId).HasColumnName("TemplateTypeID");
            });

            modelBuilder.Entity<Ticket>(entity =>
            {
                entity.ToTable("Ticket");

                entity.Property(e => e.TicketId).HasColumnName("TicketID");

                entity.Property(e => e.ApprovedAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.BidAmount).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.DateApproved).HasColumnType("datetime");

                entity.Property(e => e.DateQuote).HasColumnType("datetime");

                entity.Property(e => e.DateSubmitted)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateTicket).HasColumnType("datetime");

                entity.Property(e => e.Files).HasColumnType("xml");

                entity.Property(e => e.JobHours).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.LastDateModified).HasColumnType("datetime");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.MemberIdfrom).HasColumnName("MemberIDFrom");

                entity.Property(e => e.Pocid).HasColumnName("POCID");

                entity.Property(e => e.PriorityId).HasColumnName("PriorityID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.Subject).HasMaxLength(255);

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.ViaId).HasColumnName("ViaID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Ticket_TicketCategory");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_Ticket_Contact");

                entity.HasOne(d => d.CurrentVersion)
                    .WithMany(p => p.TicketCurrentVersions)
                    .HasForeignKey(d => d.CurrentVersionId)
                    .HasConstraintName("FK_Ticket_ProjectVersion");

                entity.HasOne(d => d.FixedInVersion)
                    .WithMany(p => p.TicketFixedInVersions)
                    .HasForeignKey(d => d.FixedInVersionId)
                    .HasConstraintName("FK_Ticket_ProjectVersion1");

                entity.HasOne(u => u.MemberFrom)
                    .WithMany(u => u.SentTickets)
                    .HasForeignKey(u => u.MemberIdfrom)
                    .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(u => u.MemberTo)
                   .WithMany(u => u.ReceivedTickets)
                   .HasForeignKey(u => u.MemberId)
                   .OnDelete(DeleteBehavior.Restrict);

                entity.HasOne(d => d.Poc)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.Pocid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Ticket_POC");

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.PriorityId)
                    .HasConstraintName("FK_Ticket_TicketPriority");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_Ticket_TicketStatus");

                entity.HasOne(d => d.TicketType)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.TicketTypeId)
                    .HasConstraintName("FK_Ticket_TicketType");

                entity.HasOne(d => d.Via)
                    .WithMany(p => p.Tickets)
                    .HasForeignKey(d => d.ViaId)
                    .HasConstraintName("FK_Ticket_TicketVia");
            });

            modelBuilder.Entity<TicketAudit>(entity =>
            {
                entity.ToTable("TicketAudit");

                entity.Property(e => e.TicketAuditId).HasColumnName("TicketAuditID");

                entity.Property(e => e.ApprovedAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.BidAmount).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.ContactId).HasColumnName("ContactID");

                entity.Property(e => e.DateApproved).HasColumnType("datetime");

                entity.Property(e => e.DateModified).HasColumnType("datetime");

                entity.Property(e => e.DateQuote).HasColumnType("datetime");

                entity.Property(e => e.DateSubmitted)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.DateTicket).HasColumnType("datetime");

                entity.Property(e => e.Files).HasColumnType("xml");

                entity.Property(e => e.IsCategoryId).HasColumnName("IsCategoryID");

                entity.Property(e => e.IsContactId).HasColumnName("IsContactID");

                entity.Property(e => e.IsMemberId).HasColumnName("IsMemberID");

                entity.Property(e => e.IsMemberIdfrom).HasColumnName("IsMemberIDFrom");

                entity.Property(e => e.IsPocid).HasColumnName("IsPOCID");

                entity.Property(e => e.IsPriorityId).HasColumnName("IsPriorityID");

                entity.Property(e => e.IsStatusId).HasColumnName("IsStatusID");

                entity.Property(e => e.IsTicketTypeId).HasColumnName("IsTicketTypeID");

                entity.Property(e => e.IsViaId).HasColumnName("IsViaID");

                entity.Property(e => e.JobHours).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.MemberIdfrom).HasColumnName("MemberIDFrom");

                entity.Property(e => e.Pocid).HasColumnName("POCID");

                entity.Property(e => e.PriorityId).HasColumnName("PriorityID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.Subject).HasMaxLength(255);

                entity.Property(e => e.TicketId).HasColumnName("TicketID");

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.UserModifiedId).HasColumnName("UserModifiedID");

                entity.Property(e => e.ViaId).HasColumnName("ViaID");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_TicketAudit_TicketCategory");

                entity.HasOne(d => d.Contact)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.ContactId)
                    .HasConstraintName("FK_TicketAudit_Contact");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.TicketAuditMembers)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_TicketAudit_Member");

                entity.HasOne(d => d.Poc)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.Pocid)
                    .HasConstraintName("FK_TicketAudit_POC");

                entity.HasOne(d => d.Priority)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.PriorityId)
                    .HasConstraintName("FK_TicketAudit_TicketPriority");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("FK_TicketAudit_TicketStatus");

                entity.HasOne(d => d.Ticket)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.TicketId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TicketAudit_Ticket");

                entity.HasOne(d => d.TicketType)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.TicketTypeId)
                    .HasConstraintName("FK_TicketAudit_TicketType");

                entity.HasOne(d => d.UserModified)
                    .WithMany(p => p.TicketAuditUserModifieds)
                    .HasForeignKey(d => d.UserModifiedId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TicketAudit_Member1");

                entity.HasOne(d => d.Via)
                    .WithMany(p => p.TicketAudits)
                    .HasForeignKey(d => d.ViaId)
                    .HasConstraintName("FK_TicketAudit_TicketVia");
            });

            modelBuilder.Entity<TicketCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("TicketCategory");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Category).HasMaxLength(75);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<TicketPriority>(entity =>
            {
                entity.HasKey(e => e.PriorityId)
                    .HasName("PK_Priority");

                entity.ToTable("TicketPriority");

                entity.Property(e => e.PriorityId).HasColumnName("PriorityID");

                entity.Property(e => e.Priority).HasMaxLength(75);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<TicketStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId);

                entity.ToTable("TicketStatus");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<TicketType>(entity =>
            {
                entity.ToTable("TicketType");

                entity.Property(e => e.TicketTypeId).HasColumnName("TicketTypeID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Type).HasMaxLength(100);
            });

            modelBuilder.Entity<TicketVium>(entity =>
            {
                entity.HasKey(e => e.ViaId);

                entity.Property(e => e.ViaId).HasColumnName("ViaID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Via).HasMaxLength(50);
            });

            modelBuilder.Entity<Tutorial>(entity =>
            {
                entity.ToTable("Tutorial");

                entity.Property(e => e.TutorialId).HasColumnName("TutorialID");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.MemberId).HasColumnName("MemberID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Title).HasMaxLength(75);

                entity.Property(e => e.TutorialTypeId).HasColumnName("TutorialTypeID");

                entity.Property(e => e.Url)
                    .HasMaxLength(100)
                    .HasColumnName("URL");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Tutorials)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Tutorial_TutorialCategory");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.Tutorials)
                    .HasForeignKey(d => d.MemberId)
                    .HasConstraintName("FK_Tutorial_Member");

                entity.HasOne(d => d.TutorialType)
                    .WithMany(p => p.Tutorials)
                    .HasForeignKey(d => d.TutorialTypeId)
                    .HasConstraintName("FK_Tutorial_TutorialType");
            });

            modelBuilder.Entity<TutorialCategory>(entity =>
            {
                entity.HasKey(e => e.CategoryId);

                entity.ToTable("TutorialCategory");

                entity.Property(e => e.CategoryId).HasColumnName("CategoryID");

                entity.Property(e => e.Category).HasMaxLength(100);

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");
            });

            modelBuilder.Entity<TutorialType>(entity =>
            {
                entity.ToTable("TutorialType");

                entity.Property(e => e.TutorialTypeId).HasColumnName("TutorialTypeID");

                entity.Property(e => e.Timestamp)
                    .IsRequired()
                    .IsRowVersion()
                    .IsConcurrencyToken()
                    .HasColumnName("timestamp");

                entity.Property(e => e.Type).HasMaxLength(50);
            });

            #region Seed_InitialData

            modelBuilder.Entity<Platform>().HasData(
               new Platform() { PlatformId = 1, Name = "MC" },
               new Platform() { PlatformId = 2, Name = "LSS" },
               new Platform() { PlatformId = 3, Name = "LM" },
               new Platform() { PlatformId = 5, Name = "Other" });

            modelBuilder.Entity<TutorialType>().HasData(
               new TutorialType() { TutorialTypeId = 1, Type = "How To Document" },
               new TutorialType() { TutorialTypeId = 2, Type  = "Video" });

            modelBuilder.Entity<PortalType>().HasData(
               new PortalType() { PortalTypeId = 1, Type = "Application" },
               new PortalType() { PortalTypeId = 2, Type = "Web Portal" });

            modelBuilder.Entity<Group>().HasData(
               new Group() { GroupId = 1, GroupName = "Accounting" },
               new Group() { GroupId = 2, GroupName = "Sales Group" },
               new Group() { GroupId = 3, GroupName = "Support Group" },
               new Group() { GroupId = 4, GroupName = "Engineering Group" });

            modelBuilder.Entity<TicketVium>().HasData(
               new TicketVium() { ViaId = 1, Via = "Phone" },
               new TicketVium() { ViaId = 2, Via = "Email" },
               new TicketVium() { ViaId = 3, Via = "Internal" });

            modelBuilder.Entity<TicketPriority>().HasData(
              new TicketPriority() { PriorityId = 1, Priority = "Low" },
              new TicketPriority() { PriorityId = 2, Priority = "Medium" },
              new TicketPriority() { PriorityId = 3, Priority = "High" },
              new TicketPriority() { PriorityId = 4, Priority = "Immediate" });

            modelBuilder.Entity<TicketType>().HasData(
              new TicketType() { TicketTypeId = 1, Type = "Support" },
              new TicketType() { TicketTypeId = 2, Type = "Consulting" },
              new TicketType() { TicketTypeId = 3, Type = "Boarding" },
              new TicketType() { TicketTypeId = 4, Type = "Test" },
              new TicketType() { TicketTypeId = 5, Type = "Other" });

            modelBuilder.Entity<TicketStatus>().HasData(
                new TicketStatus() { StatusId = 1, Status = "Assigned" },
                new TicketStatus() { StatusId = 2, Status = "Pending" },
                new TicketStatus() { StatusId = 3, Status = "On Hold" },
                new TicketStatus() { StatusId = 4, Status = "Complete" },
                new TicketStatus() { StatusId = 5, Status = "Awaiting" },
                new TicketStatus() { StatusId = 6, Status = "Scheduled" },
                new TicketStatus() { StatusId = 7, Status = "Canceled" }
                );

            modelBuilder.Entity<TicketCategory>().HasData(
                new TicketCategory() { CategoryId = 1, Category = "Training" },
                new TicketCategory() { CategoryId = 2, Category = "Call Back" },
                new TicketCategory() { CategoryId = 3, Category = "Bug" },
                new TicketCategory() { CategoryId = 4, Category = "Update System" },
                new TicketCategory() { CategoryId = 5, Category = "Feature" },
                new TicketCategory() { CategoryId = 6, Category = "Install Software" },
                new TicketCategory() { CategoryId = 7, Category = "Reporting" },
                new TicketCategory() { CategoryId = 8, Category = "Other" },
                new TicketCategory() { CategoryId = 12, Category = "Welcome" },
                new TicketCategory() { CategoryId = 13, Category = "1st Training" },
                new TicketCategory() { CategoryId = 14, Category = "2nd Training" },
                new TicketCategory() { CategoryId = 15, Category = "3rd Training" },
                new TicketCategory() { CategoryId = 16, Category = "Doc Setup" },
                new TicketCategory() { CategoryId = 17, Category = "Web App" },
                new TicketCategory() { CategoryId = 18, Category = "Web Portals" },
                new TicketCategory() { CategoryId = 19, Category = "Data Import" }
            );

            modelBuilder.Entity<Module>().HasData(
                new Module() { ModuleId = 1, ShortName = "SI", Description = "Single Investor", PlatformId = 1 },
                new Module() { ModuleId = 2, ShortName = "LS", Description = "Loan Servicing", PlatformId = 1 },
                new Module() { ModuleId = 3, ShortName = "LO", Description = "Loan Origination", PlatformId = 1 },
                new Module() { ModuleId = 4, ShortName = "TA", Description = "Trust Accounting", PlatformId = 1 },
                new Module() { ModuleId = 5, ShortName = "AC", Description = "ACH", PlatformId = 1 },
                new Module() { ModuleId = 6, ShortName = "CL", Description = "Credit Line", PlatformId = 1 },
                new Module() { ModuleId = 7, ShortName = "SR", Description = "Step Rate", PlatformId = 1 },
                new Module() { ModuleId = 8, ShortName = "PF", Description = "Pool Fund", PlatformId = 1 },
                new Module() { ModuleId = 9, ShortName = "CD", Description = "CMO & Certificates", PlatformId = 1 },
                new Module() { ModuleId = 10, ShortName = "CO", Description = "Construction", PlatformId = 1 },
                new Module() { ModuleId = 11, ShortName = "M2", Description = "Metro 2", PlatformId = 1 },
                new Module() { ModuleId = 12, ShortName = "NF", Description = "DONT FOLLOW UP", PlatformId = 1 },
                new Module() { ModuleId = 13, ShortName = "FC", Description = "Follow Ups", PlatformId = 1 },
                new Module() { ModuleId = 14, ShortName = "AP", Description = "API", PlatformId = 2 },
                new Module() { ModuleId = 16, ShortName = "LS", Description = "Loan Servicing", PlatformId = 2 },
                new Module() { ModuleId = 17, ShortName = "LO", Description = "Loan Origination", PlatformId = 2 },
                new Module() { ModuleId = 18, ShortName = "TA", Description = "Trust Accounting", PlatformId = 2 },
                new Module() { ModuleId = 19, ShortName = "AC", Description = "ACH", PlatformId = 2 },
                new Module() { ModuleId = 20, ShortName = "CL", Description = "Credit Line", PlatformId = 2 },
                new Module() { ModuleId = 22, ShortName = "PF", Description = "Pool Fund", PlatformId = 2 },
                new Module() { ModuleId = 23, ShortName = "CD", Description = "CMO & Certificates", PlatformId = 2 },
                new Module() { ModuleId = 25, ShortName = "M2", Description = "Metro 2", PlatformId = 2 },
                new Module() { ModuleId = 26, ShortName = "NF", Description = "DONT FOLLOW UP", PlatformId = 2 },
                new Module() { ModuleId = 27, ShortName = "FC", Description = "Follow Ups", PlatformId = 2 }
            );



            #endregion
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
