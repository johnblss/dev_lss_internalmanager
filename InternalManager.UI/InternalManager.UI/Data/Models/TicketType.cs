﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TicketType
    {
        public TicketType()
        {
            TicketAudits = new HashSet<TicketAudit>();
            Tickets = new HashSet<Ticket>();
        }

        public byte[] Timestamp { get; set; }
        public short TicketTypeId { get; set; }
        public string Type { get; set; }

        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
