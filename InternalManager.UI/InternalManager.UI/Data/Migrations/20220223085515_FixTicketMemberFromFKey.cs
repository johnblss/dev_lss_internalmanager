﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace InternalManager.UI.Migrations
{
    public partial class FixTicketMemberFromFKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Member",
                table: "Ticket");

            migrationBuilder.CreateIndex(
                name: "IX_Ticket_MemberIDFrom",
                table: "Ticket",
                column: "MemberIDFrom");

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Member_MemberID",
                table: "Ticket",
                column: "MemberID",
                principalTable: "Member",
                principalColumn: "MemberID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Member_MemberIDFrom",
                table: "Ticket",
                column: "MemberIDFrom",
                principalTable: "Member",
                principalColumn: "MemberID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Member_MemberID",
                table: "Ticket");

            migrationBuilder.DropForeignKey(
                name: "FK_Ticket_Member_MemberIDFrom",
                table: "Ticket");

            migrationBuilder.DropIndex(
                name: "IX_Ticket_MemberIDFrom",
                table: "Ticket");

            migrationBuilder.AddForeignKey(
                name: "FK_Ticket_Member",
                table: "Ticket",
                column: "MemberID",
                principalTable: "Member",
                principalColumn: "MemberID");
        }
    }
}
