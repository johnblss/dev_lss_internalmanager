﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace InternalManager.UI.Migrations
{
    public partial class SeedInitialData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Group",
                columns: new[] { "GroupID", "GroupName", "SecurityLevel" },
                values: new object[,]
                {
                    { (short)1, "Accounting", null },
                    { (short)2, "Sales Group", null },
                    { (short)3, "Support Group", null },
                    { (short)4, "Engineering Group", null }
                });

            migrationBuilder.InsertData(
                table: "Module",
                columns: new[] { "ModuleID", "Description", "PlatformID", "ShortName" },
                values: new object[,]
                {
                    { (short)27, "Follow Ups", (short)2, "FC" },
                    { (short)26, "DONT FOLLOW UP", (short)2, "NF" },
                    { (short)25, "Metro 2", (short)2, "M2" },
                    { (short)23, "CMO & Certificates", (short)2, "CD" },
                    { (short)22, "Pool Fund", (short)2, "PF" },
                    { (short)20, "Credit Line", (short)2, "CL" },
                    { (short)19, "ACH", (short)2, "AC" },
                    { (short)17, "Loan Origination", (short)2, "LO" },
                    { (short)16, "Loan Servicing", (short)2, "LS" },
                    { (short)14, "API", (short)2, "AP" },
                    { (short)13, "Follow Ups", (short)1, "FC" },
                    { (short)18, "Trust Accounting", (short)2, "TA" },
                    { (short)11, "Metro 2", (short)1, "M2" },
                    { (short)10, "Construction", (short)1, "CO" },
                    { (short)9, "CMO & Certificates", (short)1, "CD" },
                    { (short)8, "Pool Fund", (short)1, "PF" },
                    { (short)7, "Step Rate", (short)1, "SR" },
                    { (short)6, "Credit Line", (short)1, "CL" },
                    { (short)5, "ACH", (short)1, "AC" },
                    { (short)4, "Trust Accounting", (short)1, "TA" },
                    { (short)3, "Loan Origination", (short)1, "LO" },
                    { (short)2, "Loan Servicing", (short)1, "LS" },
                    { (short)1, "Single Investor", (short)1, "SI" },
                    { (short)12, "DONT FOLLOW UP", (short)1, "NF" }
                });

            migrationBuilder.InsertData(
                table: "Platform",
                columns: new[] { "PlatformID", "Name" },
                values: new object[,]
                {
                    { (short)5, "Other" },
                    { (short)3, "LM" },
                    { (short)1, "MC" },
                    { (short)2, "LSS" }
                });

            migrationBuilder.InsertData(
                table: "PortalType",
                columns: new[] { "PortalTypeID", "Type" },
                values: new object[,]
                {
                    { (short)1, "Application" },
                    { (short)2, "Web Portal" }
                });

            migrationBuilder.InsertData(
                table: "TicketCategory",
                columns: new[] { "CategoryID", "Category" },
                values: new object[,]
                {
                    { (short)13, "1st Training" },
                    { (short)19, "Data Import" },
                    { (short)18, "Web Portals" },
                    { (short)17, "Web App" },
                    { (short)16, "Doc Setup" },
                    { (short)15, "3rd Training" },
                    { (short)14, "2nd Training" },
                    { (short)12, "Welcome" }
                });

            migrationBuilder.InsertData(
                table: "TicketCategory",
                columns: new[] { "CategoryID", "Category" },
                values: new object[,]
                {
                    { (short)2, "Call Back" },
                    { (short)7, "Reporting" },
                    { (short)6, "Install Software" },
                    { (short)5, "Feature" },
                    { (short)4, "Update System" },
                    { (short)3, "Bug" },
                    { (short)1, "Training" },
                    { (short)8, "Other" }
                });

            migrationBuilder.InsertData(
                table: "TicketPriority",
                columns: new[] { "PriorityID", "Priority" },
                values: new object[,]
                {
                    { (short)1, "Low" },
                    { (short)2, "Medium" },
                    { (short)3, "High" },
                    { (short)4, "Immediate" }
                });

            migrationBuilder.InsertData(
                table: "TicketStatus",
                columns: new[] { "StatusID", "Status" },
                values: new object[,]
                {
                    { (short)7, "Canceled" },
                    { (short)6, "Scheduled" },
                    { (short)5, "Awaiting" },
                    { (short)3, "On Hold" },
                    { (short)2, "Pending" },
                    { (short)1, "Assigned" },
                    { (short)4, "Complete" }
                });

            migrationBuilder.InsertData(
                table: "TicketType",
                columns: new[] { "TicketTypeID", "Type" },
                values: new object[,]
                {
                    { (short)1, "Support" },
                    { (short)2, "Consulting" },
                    { (short)3, "Boarding" },
                    { (short)4, "Test" },
                    { (short)5, "Other" }
                });

            migrationBuilder.InsertData(
                table: "TicketVia",
                columns: new[] { "ViaID", "Via" },
                values: new object[,]
                {
                    { (short)1, "Phone" },
                    { (short)2, "Email" },
                    { (short)3, "Internal" }
                });

            migrationBuilder.InsertData(
                table: "TutorialType",
                columns: new[] { "TutorialTypeID", "Type" },
                values: new object[,]
                {
                    { (short)1, "How To Document" },
                    { (short)2, "Video" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Group",
                keyColumn: "GroupID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "Group",
                keyColumn: "GroupID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "Group",
                keyColumn: "GroupID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "Group",
                keyColumn: "GroupID",
                keyValue: (short)4);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)4);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)5);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)6);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)7);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)8);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)9);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)10);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)11);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)12);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)13);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)14);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)16);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)17);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)18);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)19);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)20);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)22);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)23);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)25);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)26);

            migrationBuilder.DeleteData(
                table: "Module",
                keyColumn: "ModuleID",
                keyValue: (short)27);

            migrationBuilder.DeleteData(
                table: "Platform",
                keyColumn: "PlatformID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "Platform",
                keyColumn: "PlatformID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "Platform",
                keyColumn: "PlatformID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "Platform",
                keyColumn: "PlatformID",
                keyValue: (short)5);

            migrationBuilder.DeleteData(
                table: "PortalType",
                keyColumn: "PortalTypeID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "PortalType",
                keyColumn: "PortalTypeID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)4);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)5);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)6);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)7);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)8);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)12);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)13);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)14);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)15);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)16);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)17);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)18);

            migrationBuilder.DeleteData(
                table: "TicketCategory",
                keyColumn: "CategoryID",
                keyValue: (short)19);

            migrationBuilder.DeleteData(
                table: "TicketPriority",
                keyColumn: "PriorityID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "TicketPriority",
                keyColumn: "PriorityID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "TicketPriority",
                keyColumn: "PriorityID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "TicketPriority",
                keyColumn: "PriorityID",
                keyValue: (short)4);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)4);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)5);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)6);

            migrationBuilder.DeleteData(
                table: "TicketStatus",
                keyColumn: "StatusID",
                keyValue: (short)7);

            migrationBuilder.DeleteData(
                table: "TicketType",
                keyColumn: "TicketTypeID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "TicketType",
                keyColumn: "TicketTypeID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "TicketType",
                keyColumn: "TicketTypeID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "TicketType",
                keyColumn: "TicketTypeID",
                keyValue: (short)4);

            migrationBuilder.DeleteData(
                table: "TicketType",
                keyColumn: "TicketTypeID",
                keyValue: (short)5);

            migrationBuilder.DeleteData(
                table: "TicketVia",
                keyColumn: "ViaID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "TicketVia",
                keyColumn: "ViaID",
                keyValue: (short)2);

            migrationBuilder.DeleteData(
                table: "TicketVia",
                keyColumn: "ViaID",
                keyValue: (short)3);

            migrationBuilder.DeleteData(
                table: "TutorialType",
                keyColumn: "TutorialTypeID",
                keyValue: (short)1);

            migrationBuilder.DeleteData(
                table: "TutorialType",
                keyColumn: "TutorialTypeID",
                keyValue: (short)2);
        }
    }
}
