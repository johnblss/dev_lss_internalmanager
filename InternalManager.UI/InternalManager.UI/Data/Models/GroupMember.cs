﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class GroupMember
    {
        public byte[] Timestamp { get; set; }
        public long GroupMemberId { get; set; }
        public short? GroupId { get; set; }
        public int MemberId { get; set; }

        public virtual Group Group { get; set; }
        public virtual Member Member { get; set; }
    }
}
