﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;

namespace InternalManager.UI.Pages.Tickets
{
    public class IndexModel : PageModel
    {
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public IndexModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        public IList<Ticket> Ticket { get;set; }

        public async Task OnGetAsync()
        {
            Ticket = await _context.Tickets
                .Include(t => t.Category)
                .Include(t => t.Contact)
                .Include(t => t.CurrentVersion)
                .Include(t => t.FixedInVersion)
                .Include(t => t.MemberTo)
                .Include(t => t.Poc)
                .Include(t => t.Priority)
                .Include(t => t.Status)
                .Include(t => t.TicketType)
                .Include(t => t.Via).OrderByDescending(t => t.TicketId).Take(100).ToListAsync();
        }

        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
       
    }
}
