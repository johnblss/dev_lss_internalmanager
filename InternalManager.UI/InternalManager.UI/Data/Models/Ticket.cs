﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Ticket
    {
        public Ticket()
        {
            Notes = new HashSet<Note>();
            TicketAudits = new HashSet<TicketAudit>();
        }

        public byte[] Timestamp { get; set; }
        [Display(Name = "#")]
        public long TicketId { get; set; }
        public long? ContactId { get; set; }
        public DateTime? LastDateModified { get; set; }
        public short? ViaId { get; set; }

        [Display(Name = "Type")]
        public short? TicketTypeId { get; set; }

        public short? CategoryId { get; set; }

        [Display(Name = "Priority")]
        public short? PriorityId { get; set; }

        [Display(Name = "From")]
        public int? MemberIdfrom { get; set; }

        [Display(Name = "To")]
        public int? MemberId { get; set; }
        public string MonitorList { get; set; }

        [Display(Name = "Status")]
        public short? StatusId { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Steps { get; set; }
        public string AdditionalInfo { get; set; }

        [Display(Name = "Created")]
        [DataType(DataType.Date)]
        public DateTime DateTicket { get; set; }
        public decimal? BidAmount { get; set; }
        public decimal? JobHours { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public DateTime? DateQuote { get; set; }
        public DateTime? DateApproved { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public string Files { get; set; }
        public bool PublicTicket { get; set; }
        public bool SentEmail { get; set; }
        public bool SentQuote { get; set; }

        [Display(Name = "POC")]
        public long Pocid { get; set; }
        public bool? PrePaid { get; set; }
        public int? CurrentVersionId { get; set; }
        public int? FixedInVersionId { get; set; }

        public virtual TicketCategory Category { get; set; }

        [Display(Name = "Company")]
        public virtual Contact Contact { get; set; }
        public virtual ProjectVersion CurrentVersion { get; set; }
        public virtual ProjectVersion FixedInVersion { get; set; }

        [Display(Name = "From")]
        public virtual Member MemberFrom { get; set; }


        [Display(Name = "To")]
        public virtual Member MemberTo { get; set; }

        
        [Display(Name = "POC")]
        public virtual Poc Poc { get; set; }
        public virtual TicketPriority Priority { get; set; }
        public virtual TicketStatus Status { get; set; }

        [Display(Name = "Type")]
        public virtual TicketType TicketType { get; set; }
        public virtual TicketVium Via { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<TicketAudit> TicketAudits { get; set; }
    }
}
