﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;

namespace InternalManager.UI.Pages.Tickets
{
    public class DeleteModel : PageModel
    {
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public DeleteModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Ticket Ticket { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Ticket = await _context.Tickets
                .Include(t => t.Category)
                .Include(t => t.Contact)
                .Include(t => t.CurrentVersion)
                .Include(t => t.FixedInVersion)
                .Include(t => t.MemberTo)
                .Include(t => t.Poc)
                .Include(t => t.Priority)
                .Include(t => t.Status)
                .Include(t => t.TicketType)
                .Include(t => t.Via).FirstOrDefaultAsync(m => m.TicketId == id);

            if (Ticket == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Ticket = await _context.Tickets.FindAsync(id);

            if (Ticket != null)
            {
                _context.Tickets.Remove(Ticket);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
