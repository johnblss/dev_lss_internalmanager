﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;

namespace InternalManager.UI.Pages.Tickets
{
    public class EditModel : PageModel
    {
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public EditModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Ticket Ticket { get; set; }

        public async Task<IActionResult> OnGetAsync(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Ticket = await _context.Tickets
                .Include(t => t.Category)
                .Include(t => t.Contact)
                .Include(t => t.CurrentVersion)
                .Include(t => t.FixedInVersion)
                .Include(t => t.MemberTo)
                .Include(t => t.Poc)
                .Include(t => t.Priority)
                .Include(t => t.Status)
                .Include(t => t.TicketType)
                .Include(t => t.Via).FirstOrDefaultAsync(m => m.TicketId == id);

            if (Ticket == null)
            {
                return NotFound();
            }
           ViewData["CategoryId"] = new SelectList(_context.TicketCategories, "CategoryId", "CategoryId");
           ViewData["ContactId"] = new SelectList(_context.Contacts, "ContactId", "ContactId");
           ViewData["CurrentVersionId"] = new SelectList(_context.ProjectVersions, "ProjectVersionId", "DatabaseVersion");
           ViewData["FixedInVersionId"] = new SelectList(_context.ProjectVersions, "ProjectVersionId", "DatabaseVersion");
           ViewData["MemberId"] = new SelectList(_context.Members, "MemberId", "MemberId");
           ViewData["Pocid"] = new SelectList(_context.Pocs, "Pocid", "Pocid");
           ViewData["PriorityId"] = new SelectList(_context.TicketPriorities, "PriorityId", "PriorityId");
           ViewData["StatusId"] = new SelectList(_context.TicketStatuses, "StatusId", "StatusId");
           ViewData["TicketTypeId"] = new SelectList(_context.TicketTypes, "TicketTypeId", "TicketTypeId");
           ViewData["ViaId"] = new SelectList(_context.TicketVia, "ViaId", "ViaId");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Ticket).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TicketExists(Ticket.TicketId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool TicketExists(long id)
        {
            return _context.Tickets.Any(e => e.TicketId == id);
        }
    }
}
