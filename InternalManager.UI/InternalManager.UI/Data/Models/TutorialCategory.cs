﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TutorialCategory
    {
        public TutorialCategory()
        {
            Tutorials = new HashSet<Tutorial>();
        }

        public byte[] Timestamp { get; set; }
        public short CategoryId { get; set; }
        public string Category { get; set; }

        public virtual ICollection<Tutorial> Tutorials { get; set; }
    }
}
