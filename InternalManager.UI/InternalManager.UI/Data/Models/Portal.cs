﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Portal
    {
        public byte[] Timestamp { get; set; }
        public int PortalId { get; set; }
        public long ContactId { get; set; }
        public short? PortalTypeId { get; set; }
        public string Url { get; set; }
        public DateTime? StartDate { get; set; }
        public bool Active { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual PortalType PortalType { get; set; }
    }
}
