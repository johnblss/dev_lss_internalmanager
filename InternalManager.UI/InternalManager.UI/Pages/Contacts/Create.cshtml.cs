﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using InternalManager.UI.Data;
using InternalManager.UI.Data.Models;

namespace InternalManager.UI.Pages.Contacts
{
    public class CreateModel : PageModel
    {
        private readonly InternalManager.UI.Data.LSSInternalManagerContext _context;

        public CreateModel(InternalManager.UI.Data.LSSInternalManagerContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["PlatformId"] = new SelectList(_context.Platforms, "PlatformId", "PlatformId");
            return Page();
        }

        [BindProperty]
        public Contact Contact { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Contacts.Add(Contact);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
