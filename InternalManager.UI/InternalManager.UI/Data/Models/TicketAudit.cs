﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class TicketAudit
    {
        public byte[] Timestamp { get; set; }
        public long TicketAuditId { get; set; }
        public long TicketId { get; set; }
        public int UserModifiedId { get; set; }
        public DateTime? DateModified { get; set; }
        public long? ContactId { get; set; }
        public bool IsContactId { get; set; }
        public short? ViaId { get; set; }
        public bool IsViaId { get; set; }
        public short? TicketTypeId { get; set; }
        public bool IsTicketTypeId { get; set; }
        public short? CategoryId { get; set; }
        public bool IsCategoryId { get; set; }
        public short? PriorityId { get; set; }
        public bool IsPriorityId { get; set; }
        public int? MemberIdfrom { get; set; }
        public bool IsMemberIdfrom { get; set; }
        public int? MemberId { get; set; }
        public bool IsMemberId { get; set; }
        public string MonitorList { get; set; }
        public bool IsMonitorList { get; set; }
        public short? StatusId { get; set; }
        public bool IsStatusId { get; set; }
        public string Subject { get; set; }
        public bool IsSubject { get; set; }
        public string Description { get; set; }
        public bool IsDescription { get; set; }
        public string Steps { get; set; }
        public bool IsSteps { get; set; }
        public string AdditionalInfo { get; set; }
        public bool IsAdditionalInfo { get; set; }
        public DateTime DateTicket { get; set; }
        public bool IsDateTicket { get; set; }
        public decimal? BidAmount { get; set; }
        public bool IsBidAmount { get; set; }
        public decimal? JobHours { get; set; }
        public bool IsJobHours { get; set; }
        public decimal? ApprovedAmount { get; set; }
        public bool IsApprovedAmount { get; set; }
        public DateTime? DateQuote { get; set; }
        public bool IsDateQuote { get; set; }
        public DateTime? DateApproved { get; set; }
        public bool IsDateApproved { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public bool IsDateSubmitted { get; set; }
        public string Files { get; set; }
        public bool IsFiles { get; set; }
        public bool PublicTicket { get; set; }
        public bool IsPublicTicket { get; set; }
        public bool SentEmail { get; set; }
        public bool IsSentEmail { get; set; }
        public bool SentQuote { get; set; }
        public bool IsSentQuote { get; set; }
        public long? Pocid { get; set; }
        public bool IsPocid { get; set; }
        public int TicketNoteCount { get; set; }

        public virtual TicketCategory Category { get; set; }
        public virtual Contact Contact { get; set; }
        public virtual Member Member { get; set; }
        public virtual Poc Poc { get; set; }
        public virtual TicketPriority Priority { get; set; }
        public virtual TicketStatus Status { get; set; }
        public virtual Ticket Ticket { get; set; }
        public virtual TicketType TicketType { get; set; }
        public virtual Member UserModified { get; set; }
        public virtual TicketVium Via { get; set; }
    }
}
