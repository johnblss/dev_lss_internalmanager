USE [LSSInternalManager]
GO
ALTER TABLE [dbo].[Tutorial] DROP CONSTRAINT [FK_Tutorial_TutorialType]
GO
ALTER TABLE [dbo].[Tutorial] DROP CONSTRAINT [FK_Tutorial_TutorialCategory]
GO
ALTER TABLE [dbo].[Tutorial] DROP CONSTRAINT [FK_Tutorial_Member]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_TicketVia]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_TicketType]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_TicketStatus]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_TicketPriority]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_TicketCategory]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_Ticket]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_POC]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_Member1]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_Member]
GO
ALTER TABLE [dbo].[TicketAudit] DROP CONSTRAINT [FK_TicketAudit_Contact]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_TicketVia]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_TicketType]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_TicketStatus]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_TicketPriority]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_TicketCategory]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_ProjectVersion1]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_ProjectVersion]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_POC]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_Member]
GO
ALTER TABLE [dbo].[Ticket] DROP CONSTRAINT [FK_Ticket_Contact]
GO
ALTER TABLE [dbo].[Portal] DROP CONSTRAINT [FK_Portal_PortalType]
GO
ALTER TABLE [dbo].[Portal] DROP CONSTRAINT [FK_Portal_Contact]
GO
ALTER TABLE [dbo].[POC] DROP CONSTRAINT [FK_POC_Contact]
GO
ALTER TABLE [dbo].[Note] DROP CONSTRAINT [FK_Note_Ticket]
GO
ALTER TABLE [dbo].[Note] DROP CONSTRAINT [FK_Note_Contact]
GO
ALTER TABLE [dbo].[GroupMember] DROP CONSTRAINT [FK_GroupMember_Member]
GO
ALTER TABLE [dbo].[GroupMember] DROP CONSTRAINT [FK_GroupMember_Group]
GO
ALTER TABLE [dbo].[ContactModule] DROP CONSTRAINT [FK_ContactModule_Module]
GO
ALTER TABLE [dbo].[ContactModule] DROP CONSTRAINT [FK_ContactModule_Contact]
GO
ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [FK_Contact_Platform]
GO
ALTER TABLE [dbo].[CloudHosting] DROP CONSTRAINT [FK_CloudHosting_Contact]
GO
/****** Object:  Table [dbo].[TutorialType]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TutorialType]') AND type in (N'U'))
DROP TABLE [dbo].[TutorialType]
GO
/****** Object:  Table [dbo].[TutorialCategory]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TutorialCategory]') AND type in (N'U'))
DROP TABLE [dbo].[TutorialCategory]
GO
/****** Object:  Table [dbo].[Tutorial]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tutorial]') AND type in (N'U'))
DROP TABLE [dbo].[Tutorial]
GO
/****** Object:  Table [dbo].[TicketVia]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TicketVia]') AND type in (N'U'))
DROP TABLE [dbo].[TicketVia]
GO
/****** Object:  Table [dbo].[TicketType]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TicketType]') AND type in (N'U'))
DROP TABLE [dbo].[TicketType]
GO
/****** Object:  Table [dbo].[TicketStatus]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TicketStatus]') AND type in (N'U'))
DROP TABLE [dbo].[TicketStatus]
GO
/****** Object:  Table [dbo].[TicketPriority]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TicketPriority]') AND type in (N'U'))
DROP TABLE [dbo].[TicketPriority]
GO
/****** Object:  Table [dbo].[TicketCategory]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TicketCategory]') AND type in (N'U'))
DROP TABLE [dbo].[TicketCategory]
GO
/****** Object:  Table [dbo].[TicketAudit]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[TicketAudit]') AND type in (N'U'))
DROP TABLE [dbo].[TicketAudit]
GO
/****** Object:  Table [dbo].[Ticket]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Ticket]') AND type in (N'U'))
DROP TABLE [dbo].[Ticket]
GO
/****** Object:  Table [dbo].[Templates]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Templates]') AND type in (N'U'))
DROP TABLE [dbo].[Templates]
GO
/****** Object:  Table [dbo].[ProjectVersion]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ProjectVersion]') AND type in (N'U'))
DROP TABLE [dbo].[ProjectVersion]
GO
/****** Object:  Table [dbo].[PortalType]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PortalType]') AND type in (N'U'))
DROP TABLE [dbo].[PortalType]
GO
/****** Object:  Table [dbo].[Portal]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Portal]') AND type in (N'U'))
DROP TABLE [dbo].[Portal]
GO
/****** Object:  Table [dbo].[POC]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[POC]') AND type in (N'U'))
DROP TABLE [dbo].[POC]
GO
/****** Object:  Table [dbo].[Platform]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Platform]') AND type in (N'U'))
DROP TABLE [dbo].[Platform]
GO
/****** Object:  Table [dbo].[Note]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Note]') AND type in (N'U'))
DROP TABLE [dbo].[Note]
GO
/****** Object:  Table [dbo].[Module]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Module]') AND type in (N'U'))
DROP TABLE [dbo].[Module]
GO
/****** Object:  Table [dbo].[Member]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Member]') AND type in (N'U'))
DROP TABLE [dbo].[Member]
GO
/****** Object:  Table [dbo].[GroupMember]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GroupMember]') AND type in (N'U'))
DROP TABLE [dbo].[GroupMember]
GO
/****** Object:  Table [dbo].[Group]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Group]') AND type in (N'U'))
DROP TABLE [dbo].[Group]
GO
/****** Object:  Table [dbo].[ContactModule]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContactModule]') AND type in (N'U'))
DROP TABLE [dbo].[ContactModule]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND type in (N'U'))
DROP TABLE [dbo].[Contact]
GO
/****** Object:  Table [dbo].[CloudHosting]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CloudHosting]') AND type in (N'U'))
DROP TABLE [dbo].[CloudHosting]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 2/17/2022 1:03:22 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__EFMigrationsHistory]') AND type in (N'U'))
DROP TABLE [dbo].[__EFMigrationsHistory]
GO
