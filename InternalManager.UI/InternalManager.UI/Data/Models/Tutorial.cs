﻿using System;
using System.Collections.Generic;

#nullable disable

namespace InternalManager.UI.Data.Models
{
    public partial class Tutorial
    {
        public byte[] Timestamp { get; set; }
        public long TutorialId { get; set; }
        public short? CategoryId { get; set; }
        public short? TutorialTypeId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public int? MemberId { get; set; }

        public virtual TutorialCategory Category { get; set; }
        public virtual Member Member { get; set; }
        public virtual TutorialType TutorialType { get; set; }
    }
}
